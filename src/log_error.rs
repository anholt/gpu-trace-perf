/// Simple extension of Result<> to consume an error by logging it.
///
/// Often while running we want to just log the error near the top level and be
/// done with it, without terminating the program.
use std::fmt::Display;

use anyhow::Result;
use log::error;

pub trait LogError<E> {
    fn log_error(&self);
}

impl<T, E> LogError<E> for Result<T, E>
where
    E: Display,
{
    fn log_error(&self) {
        match self {
            Ok(_) => {}
            Err(e) => {
                error!("{}", e);
            }
        }
    }
}
