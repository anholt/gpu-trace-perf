## gpu-trace-perf

This is a rust rewrite of some tooling I built for comparing
performance between different graphics driver settings on graphics
traces.  The goal is for a driver developer to be able to quickly
experiment and find how their changes affect the performance of actual
rendering.

Right now only apitrace and renderdoc traces are supported.  Each draw
call gets bracketed with GPU time elapsed queries, and we sum them
across the last frame and compare that total between the two drivers.
Thus, negative numbers in the percentage change column indicate that your
driver change is an improvement.

### Installing

```
apt-get install cargo
cargo install gpu-trace-perf
```

For apitrace traces (*.trace), you also need apitrace installed.  I
recommend having apitrace's waffle backend enabled, and
WAFFLE_PLATFORM=gbm set in the environment to not flicker windows on
the screen constantly.

For renderdoc traces (*.rdc), you need:

- python3
- renderdoc installed (`sudo apt-get install renderdoc`)
- renderdoc's python module findable from python3.

(I'd love to drop the python3 requirement by experimenting
with [pyembed](https://crates.io/crates/pyembed) to embed the python
interpreter, or ideally just [extend renderdoc-rs to support the
replay api](https://github.com/ebkalderon/renderdoc-rs/issues/2))

### Example usage

`gpu-trace-perf run --traces $HOME/src/traces-db beforedriver afterdriver`

This command will find all the traces in
[traces-db](https://gitlab.freedesktop.org/gfx-ci/tracie/traces-db/)
and run them in a loop printing stats until you feel ready to hit ^C.

The `beforedriver` and `afterdriver` arguments are scripts in your
path that set the environment to make you use your new driver, like
this:

```
#!/bin/sh

export LD_LIBRARY_PATH=$HOME/src/prefix/lib
"$@"
```

Since a traces db may be large and a change being tested may only affect a
subset of the traces, you can filter down your traces db to only those whose
stderr output is changed by some debug environment variables:

```
    gpu-trace-perf run --traces $HOME/src/traces-db beforedriver afterdriver \
        --debug-filter "NVK_DEBUG=push_dump" --debug-filter "NAK_DEBUG=print"
```


### Cross building for your embedded device

Add the following to ~/.cargo/config:

```
[target.armv7-unknown-linux-gnueabihf]
linker = "arm-linux-gnueabihf-gcc"

[target.aarch64-unknown-linux-gnu]
linker = "aarch64-linux-gnu-gcc"
```

And set up the new toolchain and build:

```
rustup target add aarch64-unknown-linux-gnu
cargo build --release --target aarch64-unknown-linux-gnu gpu-trace-perf
scp target/aarch64-unknown-linux-gnu/release/gpu-trace-perf device:bin/
```

### License

Licensed under the MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
